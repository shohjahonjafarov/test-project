package com.example.dermo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DermoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DermoApplication.class, args);
    }

}
