package com.example.dermo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultDto {

    private String name;

    private Integer age;

    private Boolean isClient;

}
