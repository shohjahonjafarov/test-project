package com.example.dermo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ResultController {

    @GetMapping("/hello/world")
    public ResultDto sayHello(){
        return ResultDto.builder()
                .name("Joka")
                .age(22)
                .isClient(true)
                .build();
    }

}
